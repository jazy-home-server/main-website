# Used to build app for deployment
# Dev stage (Mostly copied from docker/dev.dockerfile)
FROM php:7.3 AS dev

#
# Dependencies
#
RUN apt-get update \
  && apt-get install -y \
  curl \
  apt-transport-https \
  git \
  build-essential \
  libssl-dev \
  wget \
  unzip \
  bzip2 \
  libbz2-dev \
  zlib1g-dev \
  libfontconfig \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libpng-dev \
  libicu-dev \
  libxml2-dev \
  libldap2-dev \
  libmcrypt-dev \
  libzip-dev \
  python3-pip \
  fabric \
  jq \
  gnupg \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#
# Install additional php extensions
#
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
  && docker-php-ext-install -j$(nproc) \
    bcmath \
    bz2 \
    calendar \
    exif \
    ftp \
    gd \
    gettext \
    intl \
    ldap \
    #mcrypt \ NOT IN php 2.7^
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    shmop \
    soap \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    zip \
  && pecl install redis apcu \
  && docker-php-ext-enable redis apcu

#
# Install Node (with NPM), and Yarn (via package manager for Debian)
#
# https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get update \
  && apt-get install -y \
  nodejs npm \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN npm install -g yarn

#
# Install Composer and Drush
#
ENV PATH "/composer/vendor/bin:$PATH"
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /composer
ENV COMPOSER_VERSION 1.8.5

RUN php -r " \
    copy('https://getcomposer.org/installer', '/tmp/installer.php'); \
    \$signature = trim(file_get_contents('https://composer.github.io/installer.sig')); \
    \$hash = trim(hash_file('sha384', '/tmp/installer.php')); \
    if (!hash_equals(\$signature, \$hash)) { \
        unlink('/tmp/installer.php'); \
        echo 'Integrity check failed, installer is either corrupt or worse.' . PHP_EOL; \
        exit(1); \
    }" \
  && php /tmp/installer.php --no-ansi --install-dir=/usr/bin --filename=composer --version=${COMPOSER_VERSION} \
  && rm /tmp/installer.php \
  && composer --ansi --version --no-interaction

# Build stage
FROM dev AS build
WORKDIR /app
#
# Install compose dependencies
#
COPY composer.json ./
COPY composer.lock ./
RUN composer install --no-scripts --no-autoloader

#
# Install npm/yarn dependencies
#
COPY package.json ./
COPY yarn.lock ./
RUN yarn

#
# Run composer autoloader for optimization (Needs source files)
#
COPY . /app/
RUN composer dump-autoload --optimize

# Deploy stage (Rather ready for deploy)
FROM alpine:3.9 AS deploy

RUN apk add --no-cache tar openssh git

COPY --from=build /app /app/
RUN cd /app && tar cf /app.tar ./
RUN rm -rf /app/

COPY deploy-entrypoint.sh ./

CMD [ "/bin/sh", "/deploy-entrypoint.sh" ]
