# Main Website
Using:
- PHP 7.3
- Laravel 5.8

## Development
~~Development is done using a [Laravel Homestead](https://laravel.com/docs/5.8/homestead) vagrant box.~~

Development is done using docker containers. Simply running `docker-compose up` in the project directory should spin up containers and install modules. The containers will be attached to the host's port `80`.