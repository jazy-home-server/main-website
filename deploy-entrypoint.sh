#!/bin/sh
set -e
cd /

if [ -z "$KEY_FILE" ]
then
  echo "No private key set! (env KEY_FILE)"
  exit
fi

# Set proper key file with permissions
DEPLOY_KEY_FILE="$KEY_FILE"
chmod 600 "$DEPLOY_KEY_FILE"

if [ -z "$DEPLOY_DIR" ]
then
  echo "No DEPLOY_DIR env var set! Default used (/var/www/main-dev)"
  DEPLOY_DIR="/var/www/main-dev"
fi

if [ -z "$CI_COMMIT_SHA" ]
then
  # Not running within gitlab?
  mkdir /app
  tar xf /app.tar -C /app
  cd /app
  CI_COMMIT_SHA=$(git rev-parse HEAD)
  cd /
  rm -rf /app/
fi

SSH_HOST="www.jazyserver.com"
SSH_PORT=55
SSH_USER=www-deploy

DIRNAME=app.$CI_COMMIT_SHA

# Check if tar file already exists
set +e
ssh -o StrictHostKeyChecking=no -o BatchMode=yes -l $SSH_USER -p $SSH_PORT -i $DEPLOY_KEY_FILE $SSH_HOST << EOC
set -e
df "$DIRNAME.tar"
EOC
if [ $? -ne 0 ]
then
# Upload the tar file
sftp -o StrictHostKeyChecking=no -o BatchMode=yes -P $SSH_PORT -i $DEPLOY_KEY_FILE $SSH_USER@$SSH_HOST << EOC
put app.tar "$DIRNAME.tar"
EOC
fi
set -e

# Deploy the website on the remote server
ssh -o StrictHostKeyChecking=no -o BatchMode=yes -l $SSH_USER -p $SSH_PORT -i $DEPLOY_KEY_FILE $SSH_HOST << EOC
set -e
cd ~

# Extract deployment dir
mkdir -p "$DIRNAME"
tar xf "$DIRNAME.tar" -C "$DIRNAME"

# Set permissions for www-data
chmod g+w -R "$DIRNAME"

# Create links
ln -sfn /var/www/includes/shared "$DIRNAME/public/shared"
ln -sfn "$DEPLOY_DIR/env" "$DIRNAME/.env"

DIRNAME=app.$CI_COMMIT_SHA
mv "$DIRNAME/" "$DEPLOY_DIR/$DIRNAME"

# Swap the current build
cd $DEPLOY_DIR

set +e # Disable error capture for non existent link (eg first deployment)
OLDDIRNAME=\$(readlink 0)
set -e

ln -sfn $DIRNAME 0

# Clean up
if [ ! -z "\$OLDDIRNAME" ]; then
  rm -rf "\$OLDDIRNAME"
fi
rm -f "~/$DIRNAME.tar"

echo "Deployed $DIRNAME"
echo "Cleaned up \$OLDDIRNAME"
EOC



