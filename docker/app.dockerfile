FROM php:7.3-fpm

RUN sed -i '/jessie-updates/d' /etc/apt/sources.list

#
# General dependencies
#
RUN apt-get update \
  && apt-get install -y \
  curl \
  apt-transport-https \
  git \
  build-essential \
  libssl-dev \
  wget \
  unzip \
  bzip2 \
  libbz2-dev \
  zlib1g-dev \
  libfontconfig \
  libfreetype6-dev \
  libjpeg62-turbo-dev \
  libpng-dev \
  libicu-dev \
  libxml2-dev \
  libldap2-dev \
  libmcrypt-dev \
  libzip-dev \
  python3-pip \
  fabric \
  jq \
  gnupg \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

#
# Install additional php extensions
#
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
  && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
  && docker-php-ext-install -j$(nproc) \
    bcmath \
    bz2 \
    calendar \
    exif \
    ftp \
    gd \
    gettext \
    intl \
    ldap \
    #mcrypt \ NOT IN php 2.7^
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    shmop \
    soap \
    sockets \
    sysvmsg \
    sysvsem \
    sysvshm \
    zip \
  && pecl install redis apcu \
  && docker-php-ext-enable redis apcu

#
# PHP xdebug for phpunit code coverage report
#
RUN pecl install xdebug-3.1.5 \
  && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > $PHP_INI_DIR/conf.d/xdebug.ini \
  && echo "xdebug.remote_enable=on" >> $PHP_INI_DIR/conf.d/xdebug.ini \
  && echo "xdebug.remote_autostart=off" >> $PHP_INI_DIR/conf.d/xdebug.ini

#
# PHP configuration
#
# Set timezone
RUN echo "date.timezone = \"America/New_York\"" > $PHP_INI_DIR/conf.d/timezone.ini
# Increase PHP memory limit
RUN echo "memory_limit=-1" > $PHP_INI_DIR/conf.d/timezone.ini
# Set upload limit
RUN echo "upload_max_filesize = 128M\npost_max_size = 128M" > $PHP_INI_DIR/conf.d/00-max_filesize.ini

EXPOSE 9000
