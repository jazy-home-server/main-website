$('#darkToggle').click(function() {
  if ($('body').hasClass('t-dark')) {
    $('body').removeClass('t-dark');
    $('.navbar').removeClass('navbar-dark').addClass('navbar-light');
    $('.light-dark-switch').removeClass('night');
    Cookies.set('darkMode', 'false');
  } else {
    $('body').addClass('t-dark');
    $('.navbar').removeClass('navbar-light').addClass('navbar-dark');
    $('.light-dark-switch').addClass('night');
    Cookies.set('darkMode', 'true');
  }
})