<div class='light-dark-switch {{ $darkMode == "false" ? "day" : "night" }}'>
  <!-- Thank you/credits to https://codepen.io/Catagen/ -->
  <div id='darkToggle' class='time-circle'>
    <div class='sun'></div>
    <div class='moon'>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class='stars'>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class='water'></div>
  </div>
</div>