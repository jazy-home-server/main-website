<nav class="navbar {{ $darkMode == 'false' ? 'navbar-light' : 'navbar-dark' }} t-navbar sticky-top navbar-expand-md">
    <a class="navbar-brand" href="#">JL</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <!-- No other pages so no point in having a home
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            -->
            <li class="nav-item">
                <a class="nav-link" href="/packetloss/">PacketLoss</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/wedraw/">Wedraw</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Misc
                </a>
                <div class="t-dropdown-menu dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="https://old.jazyserver.com/">Old Main Website</a>
                    <a class="dropdown-item" href="https://home.jazyserver.com/">Home Server Website</a>
                    <a class="dropdown-item" href="https://vps.jazyserver.com/trans">Transmission</a>
                    <a class="dropdown-item" href="https://old.jazyserver.com/uf-scheduler/">UF Scheduler</a>
                    <!-- <div class="dropdown-divider"></div> -->
                    <a class="dropdown-item" href="/diary/">Diary</a>
                </div>
            </li>
            <!--
            <li class="nav-item">
                <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
            </li>
            -->
        </ul>
        @component('components/light-dark-switch')
        @endcomponent
    </div>
</nav>