<!doctype html>
<html lang='{{ str_replace('_', '-', app()->getLocale()) }}'>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <meta name='csrf-token' content='{{ csrf_token() }}'>
        <link rel='stylesheet' href='{{ mix('css/index.css') }}' />

        <title>@yield('title', 'Jazy site')</title>
    </head>
    <body class='{{ $darkMode == "false" ? "t-light" : "t-dark" }}'>
        @component('components/navbar')
        @endcomponent
        <div class='content'>
            @yield('content')
        </div>
        @component('components/footer')
        @endcomponent
        <script src='{{ mix('js/manifest.js') }}'></script>
        <script src='{{ mix('js/vendor.js') }}'></script>
        <script src='{{ mix('js/index.js') }}'></script>
    </body>
</html>