@extends('/layouts/master')

@section('content')
<section class='page-banner paralax paralax--stained-triforce'>
  <h1 class='page-banner__title t-page-banner__title'>
    <span>Jazy Llerena</span>
  </h1>
</section>

<section class='container text-block'>
  <p class='text-block__text text-block__text--large'>
    This is my main personal server where I do most of my development. Not necessarily web development;
    I remote desktop or ssh into my server and work on java, c++, c# (mono) projects. For me, my server is like
    my own personal cloud. That way I don't have to worry about losing my work or only being able to work at home/on my desktop.
  </p>
</section>

<section class='page-banner paralax paralax--birdies t-img'>
  <h1 class='page-banner__title t-page-banner__title'>
  </h1>
</section>

<section class='container text-block'>
  <p class='text-block__text text-block__text--large'>
    I manage 2 servers; 1 hosted at my mom's house in Miami, and the other by a paid hosting provider. This allows
    me to learn more backend technologies that go into hosting a website/webserver like apache, nginx, mysql, ect.
    Lately I have been learning Docker and containerizing services on my servers.
  </p>
</section>

<section class='page-banner paralax paralax--triforce-dark-banner t-img'>
  <h1 class='page-banner__title t-page-banner__title'>
  </h1>
</section>

<section class='container text-block'>
  <p class='text-block__text text-block__text--large'>
    For a more "professional" website, as well as my portfolio, blog, and current projects, you can visit my Wordpress website at
    <a href="https://jazyllerena.com">JazyLlerena.com</a>. Which is also hosted by me on one of my two servers.
  </p>
</section>

<section class='page-banner paralax paralax--mario-stars t-img'>
  <h1 class='page-banner__title t-page-banner__title'>
  </h1>
</section>

<section class='container text-block'>
  <p class='text-block__text text-block__text--large'>
    This webpage is currently being created with PHP Laravel, using it's template engine, blade. I am also learning how to use Quasar
    which has Vue roots. Eventually I want to use Quasar/Vue for this webpage, making it an SPA (Single Page Application). Then using Laravel
    as the backend API framework.
  </p>
</section>

<style lang='scss'>
.paralax {
  background-attachment: fixed;
  background-position: top;
  background-repeat: no-repeat;
  background-size: cover;
}

.paralax--stained-triforce {
  background-image: url({{ asset('shared/triforce-stained-glass.jpg') }});
  height: 400px;
  filter: brightness(140%);
}

.paralax--birdies {
  background-image: url({{ asset('shared/birdies.jpg') }});
  height: 400px;
}

.paralax--triforce-dark-banner {
  background-image: url({{ asset('shared/triforce-banner.jpg') }});
  height: 400px;
}

.paralax--mario-stars {
  background-image: url({{ asset('shared/mario-stars.jpg') }});
  height: 400px;
}

.page-banner {
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
}

.page-banner .page-banner__title {
  text-align: center;
  font-size: 5em;
  -webkit-text-stroke: 2px rgba(50, 50, 50, 0.3);
  /*text-shadow: 0px 0px 1px #000;*/
}

.text-block {
  margin-top: 3rem;
  margin-bottom: 3rem;
}

.text-block__text {}

.text-block__text--large {
  font-size: 1.5em;
}

body .page-banner .page-banner__title.t-page-banner__title {
  color: white;
}

body.t-dark .page-banner .page-banner__title.t-page-banner__title {
  color: white;
}
</style>
@endsection