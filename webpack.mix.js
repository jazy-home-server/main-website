const mix = require('laravel-mix');
const webpackConfig = {
  resolve: {
      alias: {
          global: path.resolve(__dirname, 'resources/sass/global/'),
          components: path.resolve(__dirname, 'resources/sass/components/')
      }
  }
};
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/js/app.js', 'public/js')
mix.js('resources/js/index.js', 'public/js')
  .webpackConfig(webpackConfig).version().sourceMaps()
  .extract(['vue', 'bootstrap', 'lodash', 'axios', 'jquery', 'js-cookie']);

//mix.sass('resources/sass/app.scss', 'public/css')
mix.sass('resources/sass/index.scss', 'public/css')
  .version().sourceMaps();
